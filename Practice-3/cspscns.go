/* Note on Array slices and maps
Array := [5]int, represent by size in number, if can't they should represent by ...
Slice := []int, to add vaue on an array we use append a = append(a, 1), hmmmmm they are useful everywere because they have function parameter
Slices have variable length, because of this they taken as an api and a standard library
because they represent an arbitrary amout of element
map := map[string]int
To add files on an array we use append keyword
*/
// programs for file copying, printing, searching, counting and sorting are done in duplication of
//program by calling the libraries and compute each element generation
package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {

	// create a scanner that take a standard input
	//Create a word that maps a string values
	scan := bufio.NewScanner(os.Stdin)
	words := make(map[string]int) // empty map

	// initialize the scanner with a spilit function of many words
	scan.Split(bufio.ScanWords)

	for scan.Scan() {
		words[scan.Text()]++
	}

	// Print amount unique words found in a file
	fmt.Println(len(words), "unique words")

	//the words in a map are indexed by a string, therefore we have sort them by the common word hay by te key or thier value by calling a library sort
	// So we form a structure to arrange it, but we have to choose

	type kv struct {
		key string
		val int
	}

	var ss []kv

	for k, v := range words {
		ss = append(ss, kv{k, v}) // slicing keys and values
	}

	// sort the map into the slice of keys and values
	// Pass the slice with a function litral which is called pass-by-value
	// Pass-by-value is parameter is passed to a function by value, which is the parameter is copied into another location of your memory
	// this function holds a consept of closure
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].val > ss[j].val
	})

	for _, s := range ss[:3] {
		fmt.Println(s.key, "appears", s.val, "times")
	}
}

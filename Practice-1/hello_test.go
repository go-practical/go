package hello

import (
	"testing"
)

func TestSay(t *testing.T) {
	subtests := []struct {
		item   []string
		result string
	}{{
		item:   []string{"hello"},
		result: "Hello!",
	}, {
		item:   []string{"sing"},
		result: "song",
	},
	}
	for _, st := range subtests {
		if s := Say(st.item); s != st.result {
			t.Errorf("expected %s, got %s", st.item, st.result)
		}

	}
}

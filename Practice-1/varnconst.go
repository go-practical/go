package basic

import (
	"fmt"
)

// Program that displays float and it value of number and thier conversion
func SimpleProg() {

	fmt.Println("This program displays an output of float and intiger numbers and thier conversion")
	a := 2
	b := 3.5

	fmt.Printf("a: %8T %[1]v\n", a)
	fmt.Printf("b: %8T %[1]v\n", b)

	fmt.Println("Conversions of float to int and vise versa")

	a = int(b)
	fmt.Printf("a: %8T %[1]v\n", a)
	b = float64(a)
	fmt.Printf("b: %8T %[1]v\n", b)
}

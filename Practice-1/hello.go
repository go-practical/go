// Package strings implements simple functions to manipulate UTF-8 encoded strings.
// Join concatenates the elements of its first argument to create a single string. 
package hello

import (
	"strings"
)

func Say(names []string) string {
	// the else condition express in return statement 
	if len(names) == 0 {
		names = []string{"world"}
	}
	return "Hello, " + strings.Join(names, ", ") + "!"
}

package basic

import (
	"fmt"
	"os"
)

// The program that displays an average that accepting a number from user in type of float and intiger
func Second() {
	var sum float64
	var n int

	fmt.Println("Please insert a number")

	for {
		var val float64
		// The basic idea here is the error message is pointing that differnt form the io reader when it points to the variable
		// &var is pointer to the variable, and stdin is a standard io reader
		_, err := fmt.Fscanln(os.Stdin, &val)
		if err != nil {
			break
		}
		// If thier is no error sum = sum+val will print and it increament
		sum += val
		n++
	}

	if n == 0 {
		fmt.Fprintln(os.Stderr, "no values")
		os.Exit(-1)
	}

	fmt.Println("The average is", sum/float64(n))
}

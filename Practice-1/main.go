// This main package have both main and test function

package main

// os.Args is a command line argument for lst of parameters,
// but we ignore the first parameter
import (
	"fmt"
	hello "nextgo/firstget"
	basic "nextgo/secondary"
	"os"
)

func main() {
	fmt.Println(hello.Say(os.Args[1:]))
	basic.SimpleProg()
	fmt.Println("To calculate an average of numbers:")
	basic.Second()
}

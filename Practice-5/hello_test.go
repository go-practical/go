// In Go we can declere functions inside other function and assign them into variables, and call then as normal functions
/*
Decipline: go over the cycle again
+ Write test
+ Make compiler pass
+ Run the test, see that it fails and check error msg
+ Write enough code to pass
+ Refactor
*/
package former

import "testing"

func TestBorn(t *testing.T) {
	name := "ABIGIYA"
	expectedOutput := "Hello, " + name

	actual, err := Born(name)

	if err != nil { // if error different form null, means have an error displays the error value
		t.Errorf("expected no error, but got %v", err)
	}

	if actual != expectedOutput { //if the actual and expexted output are not the same print an error
		t.Errorf("expected %v, but got %v", expectedOutput, actual)
	}
}

/*
Steps following
+ Define an input and expected ouput using variable decleration
+ Set condition of error raise by calling the parent function on main program
+ Define each error message outputs t.Errorf ("")
*/

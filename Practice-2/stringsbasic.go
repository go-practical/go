/* strings determine the value of bytes not the uni-code in memory allocation
The internal strings representation is a pointer and a length
When we assign strings in a memory, each characters holds the memory place as it arranges
In go programming when strings are assigned additional memory place leaved, that displays as ...
Cut strings using arrays example
	s := "Going in life forward"
	a := len(s) -> length of the string
Change the given string in upper case by
	*** s = strings.ToUpper(s)
*/
// Simple search replace program, by accepting input from user
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	// condition holds the information of the input should graterthan 3 lengthn string
	if len(os.Args) < 3 {
		fmt.Fprintln(os.Stderr, "not enough args")
		os.Exit(-1)
	}

	// The scanner a buffred io around the standard input, by default it accepts input by line
	old, new := os.Args[1], os.Args[2]
	scan := bufio.NewScanner(os.Stdin)

	// scan.Scan is a function that do the scanning that returns true
	// calling strings.Spilit is to splint the string into bunch on words in old args / slice of strings
	// calling strings.Join is helps to join the splited strings to the new one

	for scan.Scan() {
		s := strings.Split(scan.Text(), old)
		t := strings.Join(s, new)
		fmt.Println(t)
	}
}

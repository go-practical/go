package main

import "fmt"

func do(m1 *map[int]int) { // a function do have an expression of pointed to the map where it is declered as m1
	(*m1)[3] = 0
	*m1 = make(map[int]int) // make the map with the pointed value
	(*m1)[4] = 4            // 4 is the replace value of the array in memory location of 4 printed by the pointed memory location
	fmt.Println("m1", *m1)  // the print statement is prints the value of maped array
}

func main() {
	m := map[int]int{4: 1, 7: 2, 8: 3}

	fmt.Println("m", m)
	do(&m)
	fmt.Println("m", m)
}

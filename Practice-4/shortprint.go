package main

import "fmt"

func main() {
	a, b := 21, 133
	c, d := 44, 89

	fmt.Printf("%d %d\n", a, b) // dispalys the input number
	fmt.Printf("%d %d\n", c, d)
	fmt.Printf("%x %x\n", a, b)   // displays the hexa value
	fmt.Printf("%#x %#x\n", c, d) // displays the hexa value with 0x
	// fmt.Printf("%f %.3f\n", a, b)

	fmt.Println()

	fmt.Printf("%6d|%6d|\n", a, b) // displays the values with | arrangement of column
	fmt.Printf("%06d|%06d|\n", a, b)
	fmt.Printf("%-6d|%-6d|\n", a, b)
}

package main

import (
	"fmt"
	"sort"
)

// in closure function; one function call the insider function operation
func fib() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return b
	}
}

type kv struct{
	key string 
	val int
}

func slc(){
	var ss []kv 
	
	for k, v := range words {
		ss = append(ss, kv{k, v})
	}

	sort.Slice([]ss, func(i, j int) bool){
		return ss[i].val > ss[j].val
	}
}

func main() {
	f := fib()

	for x := f(); x < 100; x = f() {
		fmt.Println(x)
	}
	fmt.Println("The gap left for reason")

	s, g := fib(), fib()

	fmt.Println(s(), s(), s())
	fmt.Println(g(), g())

	fmt.Println("another output")

	fmt.Println(slc())
}
